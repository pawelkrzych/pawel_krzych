package pawelkrzych;

public class exercise04 {

    public static void main(String[] args) {

//         4. Utwórz algorytm który w zdefiniowanej 10 elementowej tablicy policzy i wyświetli średnią

        int[] myIntArray = new int[]{12, 42, 1, 22, 56, 88, 54, 12, 11, 12};
        int sum = 0;
        int average = 0;

        for (int i = 0; i < myIntArray.length; i++) {
            sum += myIntArray[i];
        }
        average = sum / myIntArray.length;
        System.out.println(average);
    }
}
