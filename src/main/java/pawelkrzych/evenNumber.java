package pawelkrzych;

import java.util.Scanner;

public class evenNumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;
        int a;
        String number;
        System.out.println("Podaj liczbę całkowitą większą od zera!");
        do {
            number = scanner.next();
            try {
                Integer.parseInt(number);
                correctNumber = true;
            } catch (NumberFormatException n) {
                System.out.println("To nie jest liczba!!!");
            }
            if (correctNumber) {
                a = Integer.parseInt(number);
                if (a <= 0) {
                    correctNumber = false;
                    System.out.println("Liczba miała być większa od zera!");
                    continue;
                }
                if (a % 2 == 0) {
                    System.out.println("liczba jest parzysta!");
                } else {
                    System.out.println("liczba jest nieparzysta!");
                }
            }

        } while (!correctNumber);


    }
}
