package pawelkrzych;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IsEven {

    public static List<Integer> evenNumbers = new ArrayList<>();
    public static List<Integer> notEvenNumbers = new ArrayList<>();
    private static final int CLOSER_AGREEMENT = 0;
    private static int sum = 0;

    public static void isItEven(int i) { //i określa ile razy ma sie wykonać pętla
        if (i == CLOSER_AGREEMENT) {
            return;
        }
        System.out.println("Podaj jeszcze " + i + " liczb");

        Scanner scanner = new Scanner(System.in);

        try {
            int a = scanner.nextInt();
            if (a > 0 && a % 2 == 0) {
                evenNumbers.add(a);
                sum += a;
            } else if (a > 0) {
                notEvenNumbers.add(a);
                sum -= a;
            } else {
                System.out.println("Liczba nieprawidłowa!!");
                IsEven.isItEven(i);
            }

            IsEven.isItEven(i - 1);
        } catch (NumberFormatException n) {
            System.out.println("Liczba nieprawidłowa!!");
            IsEven.isItEven(i);
        }
    }

    public static void main(String[] args) {
        System.out.println("Zostaniesz poproszony o podanie 3 liczb");
        isItEven(3);
        System.out.println(evenNumbers);
        System.out.println(notEvenNumbers);
        System.out.println(sum);
    }
}

