package pawelkrzych;

import java.util.InputMismatchException;
import java.util.Scanner;

public class exercise1_milosz {

    public static void main(String[] args) {

    }

    public static void method() {

        try {
            System.out.println("Sprawdzanie czy podana liczba jest liczbą parzystą");
            System.out.println("Podaj liczbę");
            Scanner scanner = new Scanner(System.in);
            int a = scanner.nextInt();
            while (a < 0) {
                System.out.println("Podaj liczbę dodatnią: ");
                a = scanner.nextInt();
            }

            scanner.close();
            if (a % 2 == 0) {
                System.out.println("Liczba jest parzysta");
            } else {
                System.out.println("Liczba jest nieparzysta");
            }

        } catch (InputMismatchException e) {
            exercise1_milosz.method();
        }
    }
}
